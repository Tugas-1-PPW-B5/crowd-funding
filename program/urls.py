from django.urls import path
from . import views
import donation.views as donation_views

app_name = "program"

urlpatterns = [
    path('', views.program_list, name="program_list"),
    path('<slug>/', views.program_details, name="program_details"),
    path('<slug>/donate/', donation_views.program_donate, name="program_donate")
]