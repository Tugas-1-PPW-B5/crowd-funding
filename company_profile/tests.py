from django.test import TestCase
from .models import Testimony
from regisform.models import Donor
import json

class TestimonyModelTest(TestCase):
    def test_default_attribute(self):
        testimony = Testimony()
        self.assertEqual(testimony.testimony, '')
        self.assertEqual(testimony.submiter, None)

    def test_testimony_string_representation(self):
        submiter = Donor(username="SUBMITER")
        submiter.save()
        testimony = Testimony(testimony="TESTIMONY", submiter=submiter)
        self.assertEqual(str(testimony), "TESTIMONY - SUBMITER")

    def test_testimony_verbose_name_plural(self):
        self.assertEqual(str(Testimony._meta.verbose_name_plural), "Testimonies")

class TestimonyPageTest(TestCase):
    def test_page_logged_in(self):
        donor = Donor(email="email@email.com")
        donor.save()
        session = self.client.session
        session['user_id'] = "USER_ID"
        session['email'] = "email@email.com"
        session.save()
        response = self.client.get('/about/')
        self.assertContains(response, 'form')

class PostTestimonyTest(TestCase):
    def test_post_new_testimony(self):
        donor = Donor(email="email@email.com")
        donor.save()
        session = self.client.session
        session['user_id'] = "USER_ID"
        session['email'] = "email@email.com"
        session.save()
        data = {
            "testimony" : "NEW TESTIMONY"
        }
        response = self.client.post('/about/', data=json.dumps(data), content_type='application/json')
        self.assertContains(response, "NEW TESTIMONY")

    def test_post_invalid_testimony(self):
        donor = Donor(email="email@email.com")
        donor.save()
        session = self.client.session
        session['user_id'] = "USER_ID"
        session['email'] = "email@email.com"
        session.save()
        data = {
            "testimony" : ""
        }
        response = self.client.post('/about/', data=json.dumps(data), content_type='application/json')
        self.assertNotContains(response, "NEW TESTIMONY")
    