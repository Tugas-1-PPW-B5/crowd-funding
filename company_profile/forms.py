from django import forms

class TestimonyForm(forms.Form):
    testimony = forms.CharField(
        widget = forms.TextInput(
            attrs = {
                'class' : 'testimony-fields',
                'id' : 'testimony_field',
                'placeholder' : 'What about you?'
            }
        )
    )
