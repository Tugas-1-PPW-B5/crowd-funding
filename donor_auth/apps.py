from django.apps import AppConfig


class DonorAuthConfig(AppConfig):
    name = 'donor_auth'
