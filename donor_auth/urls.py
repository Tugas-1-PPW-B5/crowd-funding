from django.urls import path
from . import views
import donation.views as donation_views

app_name = "donor_auth"

urlpatterns = [
    path('is_authenticated/', views.is_auth, name="is_auth"),
    path('login/', views.login, name="login"),
    path('logout/', views.logout, name="logout")
]