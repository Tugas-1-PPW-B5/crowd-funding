from django.apps import apps
from django.test import TestCase
from django.urls import resolve
from .apps import LandingPageConfig
from .views import landing_page

class LandingPageAppTest(TestCase):
    def test_apps_config(self):
        self.assertEqual(LandingPageConfig.name, 'landing_page')
        self.assertEqual(apps.get_app_config('landing_page').name, 'landing_page')

class LandingPageTests(TestCase):
    def test_landing_page_url_accessible(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_landing_page_func(self):
        found = resolve('/')
        self.assertEqual(found.func, landing_page)

    def test_landing_page_uses_correct_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'landing_page/landing_page.html')

    def test_landing_page_title_message(self):
        response = self.client.get('/')
        self.assertContains(response, 'CareShare')

    def test_profile_page_has_link_to_register_and_to_visit(self):
        response = self.client.get('/')
        html_response = response.content.decode('utf8')
        self.assertIn("Visit Us", html_response)