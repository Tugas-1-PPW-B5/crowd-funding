function createTestimony(event){
    event.preventDefault(true);
    let testimony_entry = document.getElementById("testimony_field").value;
    let csrf_token_entry = $('input[name="csrfmiddlewaretoken"]').attr('value');
    $.ajax({
        url: '',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify({
            testimony : testimony_entry
        }),
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', csrf_token_entry);
        },
        success: function(result) {
            let holder = document.getElementById("testimonies-holder");
            let new_entry = document.createElement("div");
            let entry = document.createElement("h5");
            entry.classList.add("bold");
            entry.innerText = result.testimony;
            let submiter = document.createElement("h6");
            submiter.innerText = result.submiter;
            new_entry.appendChild(submiter);
            new_entry.appendChild(entry);
            holder.prepend(new_entry);
        }
    });
}