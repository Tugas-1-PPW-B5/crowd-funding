$(document).ready(function () {
    gapi.load('auth2', function(){
      auth2 = gapi.auth2.init({
        client_id: '302110534751-9kjukh2b0rrhk1ssv2o55cgm4kllbm3q.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
    });
    let btn = document.getElementById('customBtn');
    if(btn != null){
        attachSignin(document.getElementById('customBtn'));
    }
    });
});

function attachSignin(element) {
    auth2.attachClickHandler(element, {},
        function(googleUser) {
            onSignIn(googleUser);
        }
    );
}

function onSignIn(googleUser) {
    let id_token = googleUser.getAuthResponse().id_token; 
    let csrf_token_entry = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: login_url,
        data: {
            id_token: id_token
        },
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', csrf_token_entry);
        },
        success: function (result) {
            if (result.status == 1) {
                let donate_modal = document.getElementById("DonateModal");
                if(donate_modal != null){
                    $('#DonateModal').modal({'show':'false'});
                }
                document.getElementById("log-greet").innerText = "Welcome, " + result.username;
                $('#LogInModal').modal('show');
            } 
        },
        error: function (error) {
            alert("Something went error")
        }
    })
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        let csrf_token_entry = $("[name=csrfmiddlewaretoken]").val();
        $.ajax({
            method: "POST",
            url: logout_url,
            beforeSend: function(request) {
                request.setRequestHeader('X-CSRFToken', csrf_token_entry);
            },
            success: function (result) {
                window.location.replace(result.url);
            },
            error: function (error) {
                alert("Something went error")
            }
        })
    });
}

function donate(program_name) {
    let csrf_token_entry = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: is_auth_url,
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', csrf_token_entry);
        },
        success: function (result) {
            if(result.is_auth){
                window.location.replace("../../programs/" + program_name + "/donate/");
            } else {
                $('#DonateModal').modal('show');
            }
        },
        error: function (error) {
            alert("Something went error")
        }
    });
}

function profile(){
    let csrf_token_entry = $("[name=csrfmiddlewaretoken]").val();
}