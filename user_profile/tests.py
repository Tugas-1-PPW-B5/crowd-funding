from django.test import TestCase
from django.urls import resolve
from .views import user_profile
from regisform.models import Donor

class ProfilePageTest(TestCase):
    def test_profile_page_url_no_logged_in(self):
        response = self.client.get('/profile/')
        self.assertEqual(response.status_code, 302)

    def test_profile_page_url_logged_in(self):
        donor = Donor(email="email@email.com")
        donor.save()
        session = self.client.session
        session['user_id'] = "USER_ID"
        session['email'] = "email@email.com"
        session.save()
        response = self.client.get('/profile/')
        self.assertTemplateUsed(response, 'user_profile/profile.html')

    def test_using_profile_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, user_profile)

