from django.apps import apps
from django.test import TestCase, Client
from django.urls import resolve
from .apps import NewsConfig
from .views import *
from .models import *

class NewsAppTest(TestCase):
    def test_apps_config(self):
        self.assertEqual(NewsConfig.name, 'news')
        self.assertEqual(apps.get_app_config('news').name, 'news')

class NewsUnitTest(TestCase):
    def setUp(self):
        return News.objects.create(
            title="News Title", 
            content="Lorem ipsum dolor sit amet")
            
    def test_news_url_status_is_exist(self):
        response = Client().get('/news/')
        self.assertEqual(response.status_code, 200)

    def test_news_url_status_func_working(self):
        found = resolve('/news/')
        self.assertEqual(found.func, news_page)

    def test_news_model_can_create_news(self):
        count_news = News.objects.all().count()
        self.assertEqual(count_news, 1)

    def test_news_model_can_print(self):
        news = self.setUp()
        self.assertEqual('News Title', news.__str__())

    def test_normal_news_content(self):
        news = self.setUp()
        self.assertEqual(news.content, "Lorem ipsum dolor sit amet")
