from django.apps import AppConfig

class RegisformConfig(AppConfig):
    name = 'regisform'
