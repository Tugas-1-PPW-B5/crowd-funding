# Generated by Django 2.1.1 on 2018-10-18 01:47

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Regis',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=30)),
                ('ttl', models.DateField()),
                ('email', models.CharField(max_length=30)),
                ('password', models.CharField(max_length=20)),
                ('confpassword', models.CharField(max_length=20)),
            ],
        ),
    ]
