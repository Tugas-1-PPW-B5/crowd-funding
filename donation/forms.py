from django import forms

class DonationForm(forms.Form):
	anon = forms.BooleanField(
		label = "Anonymous", 
		required= False,
		widget = forms.CheckboxInput(
			attrs = {
				'class' : 'checkbox',
				'required' : False
			}
		)
	)
	amount = forms.IntegerField(
		label = "Donation Amount", 
		widget = forms.NumberInput(
			attrs = {
				'class' : 'form-control',
			}
		)
	)
